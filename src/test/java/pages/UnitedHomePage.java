package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class UnitedHomePage {
    public UnitedHomePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "(//ul[@role='tablist'])[2]/li")
    public List<WebElement> bookTravelMenuItems;

    @FindBy(xpath = "//div[@role='tablist']/a")
    public List<WebElement> mainMenuNavItems;

    @FindBy(xpath = "(//label[@for='roundtrip']/*)[1]")
    public WebElement roundTripText;

    @FindBy(xpath = "(//label[@for='roundtrip']/*)[2]")
    public WebElement roundTripInput;

    @FindBy(xpath = "(//label[@for='roundtrip']/*)[3]")
    public WebElement roundTripButton;

    @FindBy(xpath = "(//label[@for='oneway']/*)[1]")
    public WebElement onewayTripText;

    @FindBy(xpath = "(//label[@for='oneway']/*)[2]")
    public WebElement onewayTripInput;

    @FindBy(xpath = "(//label[@for='oneway']/*)[3]")
    public WebElement onewayTripButton;

    @FindBy(xpath = "//label[@for='award']")
    public WebElement bookWithMilesLabel;

    @FindBy(id="award")
    public WebElement bookWithMilesInput;

    @FindBy(xpath = "//label[@for='flexibleDates']")
    public WebElement flexibleDatesLabel;

    @FindBy(id = "flexibleDates")
    public WebElement flexibleDatesInput;

    @FindBy(xpath = "//input[@id='bookFlightOriginInput']")
    public WebElement fromInput;

    @FindBy(xpath = "//input[@id='bookFlightDestinationInput']")
    public WebElement toInput;

    @FindBy(css = "#autocomplete-item-0 button")
    public WebElement autoCompleteOption;

    @FindBy(xpath = "//input[@id='DepartDate']")
    public WebElement datesInput;

    @FindBy(css = "div[id='passengerSelector']")
    public WebElement passengerInput;

    @FindBy(xpath = "//input[contains(@aria-label, 'Adults')]")
    public WebElement travelersAdultsInput;

    @FindBy(id = "cabinType")
    public WebElement classSelector;

    @FindBy(css = "ul[aria-labelledby='cabinDescriptor']>li")
    public List<WebElement> listOffClassOptions;

    @FindBy(xpath = "//button[@aria-label='Find flights']")
    public WebElement findFlightsButton;

    @FindBy(xpath = "//h2[1]")
    public WebElement departureFromToHeader;

//    @FindBy(css = "button[aria-label='Add one more Adult']")
//    public WebElement lessButton;
//
//    @FindBy(css = "button[aria-label='Add one more Adult']")
//    public WebElement lessButton;


}
