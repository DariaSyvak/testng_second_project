package scripts;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class UnitedAirlinesTest extends Base {

    @Test(priority = 1, description = "Test Case 1: Validate \"Main menu\" navigation items")
    public void mainMenu() {
        driver.get("https://www.united.com/en/us");
        String[] navigationTexts = {"BOOK", "MY TRIPS", "TRAVEL INFO", "MILEAGEPLUS® PROGRAM", "DEALS", "HELP"};

        int index = 0;
        for (WebElement navigationItem : unitedHomePage.mainMenuNavItems) {
            assertTrue(navigationItem.isDisplayed());
            assertEquals(navigationItem.getText(), navigationTexts[index++]);
        }
    }

    @Test(priority = 2, description = "Test Case 2: Validate \"Book travel menu\" navigation items")
    public void bookTravelMenu() {
        driver.get("https://www.united.com/en/us");
        String[] navigationTexts = {"Book", "Flight status", "Check-in", "My trips"};

        int index = 0;
        for (WebElement navigationItem : unitedHomePage.bookTravelMenuItems) {
            assertTrue(navigationItem.isDisplayed());
            assertEquals(navigationItem.getText(), navigationTexts[index++]);

        }

    }

    @Test(priority = 3, description = "Test Case 3: Validate \"Round-trip\" and \"One-way\" radio buttons")
    public void roundTrip() {
        driver.get("https://www.united.com/en/us");
        assertEquals(unitedHomePage.roundTripText.getText(), "Roundtrip");
        assertEquals(unitedHomePage.onewayTripText.getText(), "One-way");

        assertTrue(unitedHomePage.roundTripText.isDisplayed());
        assertTrue(unitedHomePage.onewayTripText.isDisplayed());

        assertTrue(unitedHomePage.roundTripInput.isEnabled());
        assertTrue(unitedHomePage.onewayTripInput.isEnabled());

        assertTrue(unitedHomePage.roundTripInput.isSelected());
        assertFalse(unitedHomePage.onewayTripInput.isSelected());

        //click one way
        unitedHomePage.onewayTripButton.click();

        assertFalse(unitedHomePage.roundTripInput.isSelected());
        assertTrue(unitedHomePage.onewayTripInput.isSelected());

    }

    @Test(priority = 4, description = "Test Case 4: Validate \"Book with miles\" and \"Flexible dates\" checkboxes")
    public void BookWithMiles() {
        driver.get("https://www.united.com/en/us");

        assertEquals(unitedHomePage.bookWithMilesLabel.getText(), "Book with miles");
        assertEquals(unitedHomePage.flexibleDatesLabel.getText(), "Flexible dates");

        assertTrue(unitedHomePage.bookWithMilesLabel.isDisplayed());
        assertTrue(unitedHomePage.flexibleDatesLabel.isDisplayed());

        assertTrue(unitedHomePage.bookWithMilesInput.isEnabled());
        assertTrue(unitedHomePage.flexibleDatesInput.isEnabled());
        assertFalse(unitedHomePage.bookWithMilesInput.isSelected());
        assertFalse(unitedHomePage.flexibleDatesInput.isSelected());

        unitedHomePage.bookWithMilesLabel.click();
        unitedHomePage.flexibleDatesLabel.click();

        assertTrue(unitedHomePage.bookWithMilesInput.isSelected());
        assertTrue(unitedHomePage.flexibleDatesInput.isSelected());

        unitedHomePage.bookWithMilesLabel.click();
        unitedHomePage.flexibleDatesLabel.click();

        assertFalse(unitedHomePage.bookWithMilesInput.isSelected());
        assertFalse(unitedHomePage.flexibleDatesInput.isSelected());


    }

    @Test(priority = 5, description = "Validate One-way ticket search results \"from Chicago, IL, US (ORD) to Miami, FL, US (MIA)")
    public void trip() {
        driver.get("https://www.united.com/en/us");
        unitedHomePage.onewayTripButton.click();
        unitedHomePage.fromInput.clear();
        unitedHomePage.fromInput.sendKeys("Chicago, IL, US (ORD");
        unitedHomePage.toInput.clear();
        unitedHomePage.toInput.sendKeys("Miami, FL, US (MIA");
        unitedHomePage.autoCompleteOption.click();

        unitedHomePage.datesInput.clear();
        unitedHomePage.datesInput.sendKeys("Jun 30");
        unitedHomePage.passengerInput.click();
        unitedHomePage.travelersAdultsInput.sendKeys("2");
        unitedHomePage.classSelector.click();
        unitedHomePage.listOffClassOptions.get(2).click();
        unitedHomePage.findFlightsButton.click();

        assertEquals(unitedHomePage.departureFromToHeader.getText(),
                "Depart: Chicago, IL, US to Miami, FL, US");

    }
}